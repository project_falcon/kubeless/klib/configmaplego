package configmaplego

import (
	"encoding/json"

	"gitlab.com/project_falcon/kubeless/lib/payload"
	apiV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// clusterTaskName = "cluster-task"
	ecrLoginName = "ecr-login"
	IngressList = "ingress-list"
	BaseAuth = "baseauth"
	ExtraEnv = "extraenv"
	Suspend = "suspend"
)

func GetECRLogin(namespace string) apiV1.ConfigMap {
	data := make(map[string]string)
	data["config.json"] = `{ "credsStore": "ecr-login" }`

	ecrLogin := apiV1.ConfigMap{
		ObjectMeta: metaV1.ObjectMeta{
			Name:      ecrLoginName,
			Namespace: namespace,
		},
		Data: data,
	}

	return ecrLogin
}

//GetClusterTask add configmap about JSON TASK
func GetConfigMap(name string, namespace string, buffer payload.Buffer) apiV1.ConfigMap {

	task, _ := json.Marshal(buffer.Task)
	// if err != nil {
	// 	tools.HandlerMessage(apiEvent, service, fmt.Sprintf("there is problem to Marshal apiEvent.Buffor.Task err: '%v'", err.Error()), 500, tools.WarnColor)
	// 	return err
	// }

	data := make(map[string]string)
	data[name] = string(task)

	clusterTask := apiV1.ConfigMap{
		ObjectMeta: metaV1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Data: data,
	}

	return clusterTask
}

func GetBaseConfigMap(name string, namespace string, m map[string]string) apiV1.ConfigMap {

	clusterTask := apiV1.ConfigMap{
		ObjectMeta: metaV1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Data: m,
	}

	return clusterTask
}
