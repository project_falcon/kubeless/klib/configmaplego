module gitlab.com/project_falcon/kubeless/klib/configmaplego

go 1.14

require (
	gitlab.com/project_falcon/kubeless/lib/payload v1.52.0
	k8s.io/api v0.19.4
	k8s.io/apimachinery v0.19.4
)
